# courses++

Build Platform   | Status (tests only)
---------------- | ----------------------
Linux x64        | ![Linux x64](https://img.shields.io/bitbucket/pipelines/OneWhiteSpirit/courses.svg)

### Generate Docker image (for bitbucket pipelines)
 - write Dockerfile
 - call ```sudo systemctl start docker```
 - call ```sudo docker build -t onedc138/gamedevcourses .```
 - call ```sudo docker push onedc138/gamedevcourses```

Dockerfile content:

```sh
FROM ubuntu:bionic-20180821s

RUN apt update -y
RUN apt upgrade -y
RUN apt install -y g++
RUN apt install -y make
RUN apt install -y cmake
RUN apt install -y gcc-mingw-w64
RUN apt install -y g++-mingw-w64
RUN apt install -y clang
RUN apt install -y wine-stable
RUN apt install -y git
RUN apt install -y libsdl2-dev
```
